# Restaurant Reviews

## Getting started:

```
npm install
```

In a terminal, check the version of Python you have: 
```
python -V
```
If you have Python 2.x, spin up the server with: 
```
python -m SimpleHTTPServer 8000
```
(or some other port, if port 8000 is already in use.) For Python 3.x, you can use: 
```
python3 -m http.server 8000
``` 
If you don't have Python installed, navigate to Python's [website](https://www.python.org/) to download and install the software.

With your server running, visit the site: `http://localhost:8000`, and look around for a bit to see what the current experience looks like.

### Then

Enter to `server-to-mws` folder, and in a terminal install Sails Server (for data):
```
npm i sails -g
```
Now run the server with data:
```
sails lift
```

### Screenshoot:
![restaurant1.jpg](https://bitbucket.org/repo/5q66Ekg/images/1249870652-restaurant1.jpg)



