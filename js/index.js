//SERVICE WORKER
if ('serviceWorker' in navigator) {
  window.addEventListener('load', function() {
    navigator.serviceWorker.register('/sw.js').then(function(registration) {
      // Registration was successful
      console.log('ServiceWorker registration successful with scope: ', registration.scope);
    }, function(err) {
      // registration failed :(
      console.log('ServiceWorker registration failed: ', err);
    });
  });
}
var messages;
    fetch('http://localhost:1337/restaurants')
      .then(response => JSON.parse(response))
      .then(restaurants = (restaurant) => {
            messages = restaurant;
    });

console.log(messages);

//IDB
var dbPromise = idb.open('database', 1, function(upgradeDb) {
   var store = upgradeDb.createObjectStore("restaurants", {
     keyPath: 'id'
   });
});

//ADD DATA

dbPromise.then(function(db) {
    var tx = db.transaction('restaurants', 'readwrite');
    var keyValStore = tx.objectStore('restaurants');
    keyValStore.put('bar', 'foo');
    return tx.complete;
}).then(function() {
    console.log('Added foo:bar to keyval');
});
